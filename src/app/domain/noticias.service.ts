import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NoticiasService {
  private noticias: string[] = [];

  constructor() { }

  agregar(noticia: string): boolean{
    this.noticias.push(noticia);
    return true;
  }

  buscar(){
    return this.noticias;
  }

  eliminar(noticia: string): boolean{
    this.noticias = this.noticias.map(n => {if(n !== noticia){return n}}).filter(n => n !== undefined);
    return true;
  }
}
