import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { NativeScriptCommonModule } from '@nativescript/angular';
import { Page1Component } from './page1.component';
import { Page1RoutingModule } from './page1-routing.module';



@NgModule({
  declarations: [
    Page1Component
  ],
  imports: [
    NativeScriptCommonModule,
    Page1RoutingModule
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class Page1Module { }
