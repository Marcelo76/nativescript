import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
//import { SearchFormComponent } from "./search/search-form.component";
//import { ListadoComponent } from './listado/listado.component';
//import { DetalleComponent } from './detalle/detalle.component';
//import { Page1Component } from './page1/page1.component';
//import { Page2Component } from './page2/page2.component';

@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        AppRoutingModule,
        NativeScriptModule,
        NativeScriptUISideDrawerModule
    ],
    declarations: [
        AppComponent,
        //SearchFormComponent
        //ListadoComponent,
        //DetalleComponent,
        //Page1Component,
        //Page2Component
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
