import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { NoticiasService } from "../domain/noticias.service";
import { borderTopRightRadiusProperty, View, Color } from "tns-core-modules/ui/page";
import * as dialogs from 'tns-core-modules/ui/dialogs';

@Component({
    selector: "Search",
    templateUrl: "./search.component.html",
    //providers:[NoticiasService]
})
export class SearchComponent implements OnInit {
    resultados: string[] = [];
    @ViewChild("layout", { static: false}) layout: ElementRef;
    public imagenes: string[] = [
        'res://icon',
        'https://i.pinimg.com/236x/4e/40/dd/4e40ddd11beb9ba671a0b59948861afb--motion-design-ben.jpg',
        '~/images/1.png'];


    constructor(public _noticiasService: NoticiasService){
        // Use the component constructor to inject providers.
    }

    ngOnInit(): void {
        // Init your component properties here.
        if(this._noticiasService.buscar().length == 0){
            this._noticiasService.agregar('Noticia 1');
            this._noticiasService.agregar('Noticia 2');
            this._noticiasService.agregar('Noticia 3');
        }
        this.resultados = this._noticiasService.buscar();
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    buscar(e: string){
        if(e === ''){
            this.resultados = this._noticiasService.buscar();
        }else{
            this.resultados = this._noticiasService.buscar().filter(n => n.indexOf(e) >= 0);
        }

        const layout = <View>this.layout.nativeElement;
        layout.animate({
          backgroundColor: new Color('rgb(59, 175, 218)'),
            duration: 300,
            delay: 150
        }).then(() => layout.animate({
            backgroundColor: new Color('white'),
            duration: 300,
            delay: 150
        })).then(()=>layout.animate({
            rotate: 360,
            duration: 300,
            delay: 150
        })).then(()=>layout.animate({
            rotate: 360,
            duration: 300,
            delay: 150
        }))
    }

    showOptions(e){
      const layout = <View>this.layout.nativeElement;
      layout.animate({
          backgroundColor: new Color('rgb(59, 175, 218)'),
          duration: 1000,
          delay: 150
      }).then(() =>
          layout.animate({
              backgroundColor: new Color('white'),
              delay: 150,
              duration: 1000
          })
      ).then(() => {
          let nuevo = 'Noticia ' + Math.floor((Math.random() * 97) + 3);
          dialogs.action("Selecciona","Cancelar",[`Borrar ${e}`,`Archivar ${nuevo}`])
          .then((result) => {
              console.log(result);
              if(result === `Borrar ${e}`){
                this._noticiasService.eliminar(e);

              }else if(result === `Archivar ${nuevo}`){
                this._noticiasService.agregar(nuevo);

              }
          }).then(()=>{
            this.resultados = this._noticiasService.buscar();
          })
      });

    }

    loadImage(){
        return this.imagenes[Math.floor(Math.random() * 3)];
    }
}
