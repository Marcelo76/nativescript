import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ns-search-form',
    moduleId: module.id,
    template: `
    <FlexboxLayout flexDirection="row">
        <TextField #texto="ngModel" [(ngModel)]="textFieldValue" hint="Ingresar texto..." required minlen="4"></TextField>
        <Label *ngIf="texto.hasError('required')" text="*"></Label>
        <Label *ngIf="!texto.hasError('required') && texto.hasError('minlen')" text="4+"></Label>
    </FlexboxLayout>
    <Button text="Buscar" (tap)="onButtonTap()" *ngIf="texto.valid"></Button>
    `
})
export class SearchFormComponent implements OnInit {
    public textFieldValue: string;
    @Output() search: EventEmitter<string> = new EventEmitter();
    @Input() textoInicial: string;

  constructor() { }

  ngOnInit(): void {
      this.textFieldValue = this.textoInicial;
  }

  onButtonTap(): void{
    console.log(this.textFieldValue);
    if(this.textFieldValue.length > 0){
        this.search.emit(this.textFieldValue);
    }
  }


}
