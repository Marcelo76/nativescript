import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { device } from "tns-core-modules/platform";

@Component({
    selector: "Home",
    templateUrl: "./home.component.html"
})
export class HomeComponent implements OnInit {
    public plataforma: string = "No pude detectar tu dispositivo.";
    public msg: string;


    constructor() {
        // Use the component constructor to inject providers.        
    }

    ngOnInit(): void {
        // Init your component properties 
        console.log(device.os);
        if(device.os === 'Android'){
            this.plataforma = "Estás en un dispositivo Android";
            this.msg = "Bienvenido a Android";
        }
        if(device.os === 'IOS'){
            this.plataforma = "Estás en un dispositivo IOS";
        }
    }

    onDrawerButtonTap(): void {        
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }
}
